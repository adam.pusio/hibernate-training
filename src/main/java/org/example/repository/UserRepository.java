package org.example.repository;


import org.example.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.Optional;

public class UserRepository extends EntityRepository<User> {
    public UserRepository(SessionFactory sessionFactory) {
        super(sessionFactory, User.class);
    }

    public Optional<User> getUserByLoginAndPassword(String login, String password){
        Session session = getSessionFactory().openSession();
        Query<User> query = session.createQuery(
                "From User u Where u.userName = :username and u.password = :password",
                User.class);
        query.setParameter("username", login);
        query.setParameter("password", password);
        User singleResult = query.getSingleResult();
        session.close();
        return Optional.ofNullable(singleResult);
    }
}
