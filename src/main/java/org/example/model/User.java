package org.example.model;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    @Id
    private UUID id;
    @Column(unique = true, nullable = false)
    private String username;

    //password stored in plaintext
    //NEVER EVER DO THIS ON PROD
    @Column(nullable = false)
    private String password;
}
