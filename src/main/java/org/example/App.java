package org.example;

import org.example.model.User;
import org.example.repository.UserRepository;
import org.example.service.UserService;
import org.hibernate.SessionFactory;

import java.util.Arrays;
import java.util.Scanner;

public class App
{
    public static void main( String[] args ) {
        SessionFactory sessionFactory = new HibernateFactory().getSessionFactory();
        UserService userService = new UserService(new UserRepository(sessionFactory));

        System.out.println("CHOSE AN OPTION:");
        Arrays.stream(LoginMenuOptions.values())
                .map(value -> value.getNumber() + ". " + value.getText())
                .forEach(System.out::println);

        Scanner scanner = new Scanner(System.in);
        LoginMenuOptions option = LoginMenuOptions.getByNumber(scanner.nextInt());

        User user = null;
        switch (option) {
            case REGISTER:
                user = userService.regiserNewUser();
            case LOGIN:
                user = userService.login();
            case EXIT:
                //exit
        }

        System.out.println("ZALOGOWALES SIE JAKO USER!" + user);
    }
}
