package org.example;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum LoginMenuOptions {
    REGISTER(1, "Register new user"),
    LOGIN(2, "login as existing user"),
    EXIT(3, "exit");

    private final int number;
    private final String text;

    LoginMenuOptions(int number, String text) {
        this.number = number;
        this.text = text;
    }

    public static LoginMenuOptions getByNumber(int number) {
        Optional<LoginMenuOptions> first = Arrays.stream(LoginMenuOptions.values())
                .filter(option -> option.getNumber() == number)
                .findFirst();

        return first.orElseThrow(OptionNotRecognizedException::new);
    }
}
